// import logo from './logo.svg';
import './App.css';
import {useEffect, useState, Component} from "react";
import { Button, Box, useTheme,Typography ,
  TableHead, TableRow, TableBody, TableCell, 
  TablePagination
  ,  TableContainer,
  Table,
  TableFooter,
} from "@mui/material";
import Header from "../src/components/Header";
import IconButton from "@mui/material/IconButton";
import React from 'react';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import { MdArrowBack } from 'react-icons/md';
import { tokens } from "../src/theme";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";

import { makeStyles } from '@material-ui/core/styles';
import { DataTaskView } from "../src/data/mockData";
// import Table from 'react-bootstrap/Table';

import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
// import Text from 'react-native';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import DescriptionIcon from '@mui/icons-material/Description';

const useStyles = makeStyles((theme) => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
  button: {
    marginTop: theme.spacing(2),
  },
  tableCell: {
    borderBottom: "none",
  },
  tableHeader: {
    backgroundColor: "#e1e2fe",
  },
}));

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box 
    sx={{ flexShrink: 0, ml: 2.5 }}
    >
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

function App() {


  const [shown, setShown] = useState(false);
  const [srcurl,setSrcurl] = useState('');
  const [publicToken, setPublicToken] = useState('');
  const [transaction, setTransaction] = useState(false); 
  ///
  const [dataTransaction, setDataTransaction] = useState(0);
  const [textDisplay, setTextDisplay] = useState("Lấy giao dịch")
  // no fix obj save


  // init information transaction

  // const [accountname, setAccountname] = useState('');
  // const [accountnumber, setAccountnumber] = useState('');
  // const [amount, setAmount] = useState('');
  // const [description, setDescription] = useState('');
  // const [transactionDateTime, setTransactionDateTime] = useState('');
  // const [runningBalance, setRunningBalanece] = useState('');

  // const [form, setForm] = useState({
  //   index: '',
  //   name: '',
  //   number: '',
  //   amount: '',
  //   description: '',
  //   transactionDateTime: '',
  //   runningBalance: '',
  // })
  // let dataArray = [
  //   {id: '9999'}
  // ];
  const [dataArray, setDataArray] = useState(
    [

    ]
  )

  // const {
  //   index,
  //   name, number, amount, description, transactionDateTime, runningBalance
  // } = form

  // const setField = (field, value) => {
  //   setForm({
  //     ...form,
  //     [field]: value
  //   })

  //   // if (!!errors[field])
  //   //   setErrors({
  //   //     ...errors,
  //   //     [field]: null
  //   //   })
  // }

  const [disableClick, setDisableClick] = useState(true);
  const [ isAlertVisible, setIsAlertVisible ] = useState(false);
  const [ isAlertVisible2, setIsAlertVisible2 ] = useState(false);
  // const [ isAlertVisible3, setIsAlertVisible3 ] = useState(false);
  const [ accountName, setAccountName] = useState('');
  const handleButtonClick = () => {
      setIsAlertVisible(true);

        setTimeout(() => {
            setIsAlertVisible(false);
        }, 3000);
  }

  const handleButtonClick2 = () => {
    setIsAlertVisible2(true);

      setTimeout(() => {
          setIsAlertVisible2(false);
      }, 3000);
}
// const handleButtonClick3 = () => {
//   setIsAlertVisible3(true);

//     setTimeout(() => {
//         setIsAlertVisible3(false);
//     }, 3000);
// }
  // const onClear = (e) => {
  //   setForm({
  //     name: '',
  //     number: '',
  //     amount: '',
  //     description: '',
  //     transactionDateTime: '',
  //     runningBalance: '',


  //   })
  //   // e.target.reset();
  //   // setForm=null

  // }

  // end init 
  function refreshPage(){
    window.location.reload();
} 
    async function handleExchangeToken(publicToken) {
      try {
        const response = await axios.post(
          `http://localhost:8000/bankhub/exchangeToken?publicToken=${publicToken}`
        );
        console.log(response.data);
        const aToken = response.data;
        if(aToken){

            localStorage.setItem("accessToken", aToken);
            refreshPage();
 
        }

      }
      catch (error){
        console.log(error);
      }
      
    }



    function handleAccessToken(accessToken){
      axios.post(`http://localhost:8000/bankhub/transaction?accessToken=${accessToken}`, {
        // firstName: 'Fred',
        // lastName: 'Flintstone'
      })
      .then(function (response) {

        

        // console.log(response.data.transactions);
 
          setDataArray(response.data.transactions);
          setAccountName(response.data.accounts[0].accountName);
        
          // console.log(dataArray);
          // console.log(DataTaskView);
          console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
    }


    useEffect(() => {
      let aToken = localStorage.getItem("accessToken");
  
      if(aToken && (disableClick ===true)){
        setDisableClick(false); 
        handleButtonClick();
      }
      if(disableClick === false){
        setTextDisplay("Lấy thông tin giao dịch");
        // refreshPage();  
      }

      const handleMessage = (event) => {
        console.log(event);
        if(event.origin === "https://dev.link.bankhub.dev"){
          const data = JSON.parse(event.data);
  
          console.log(data.data.publicToken);
          setPublicToken(data.data.publicToken);

          // console.log(data.data);
          if(data.data.loading === false){
            setShown(false);
            console.log("Close iframe");


          }
          // if(data.data.publicToken){
          //   refreshPage();
          // }
        }



        
      }
 
      window.addEventListener('message', handleMessage );

      if(publicToken){
        handleExchangeToken(publicToken);
      }

      return () => {
        window.removeEventListener('message',handleMessage);
      }

  }, [publicToken]);

  // const grantToken2 = 0;
  const handleClick =  (e) => {

    axios.get('http://localhost:8000/bankhub/token', {
      params: {
        ID: 12345
      }
    })
    .then(function (response) {
      console.log(response.data.iframeurl);
      setSrcurl(response.data.iframeurl);
    }) ;
    if(shown === false){


    setShown(true);
  }


  


 };

 const handleClickTransaction =  (e) => {
  let aToken = localStorage.getItem("accessToken");
  if(aToken){

    handleAccessToken(aToken);
    setTransaction(true);
    handleButtonClick2();

  }



  

}


 const IframeModal = (props) => {
  return <div>
    <iframe
        className = "App"
        title={props.src}
        src={props.src}

    />
  </div>

}

  
  const classes = useStyles();
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const columns = [
    { field: "id", headerName: "ID", flex: 0.5 },
    {
      field: "name",
      headerName: "Họ và Tên",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      flex: 1,
    },
    {
      field: "email",
      headerName: "Email",
      flex: 1,
    },
    {
      field: "type",
      headerName: "Loại nhân viên",
      flex: 1,
    },
    {
      field: "vehicle",
      headerName: "Phương Tiện",
      flex: 1,
    },
    {
      field: "task",
      headerName: "Nhiệm vụ",
      flex: 1,
    },
    {
      field: "status",
      headerName: "Tình trạng",
      flex: 1,
    },
  ];

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - dataArray.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  console.log(dataArray)
  // const dataTrans= JSON.stringyfi(dataArray);
  return (
    // <div> </div>
    <Box>

      <div className="headerglassmorphism">
      <Header
              
              title="XEM THÔNG TIN GIAO DỊCH"
              subtitle="Liên kết ngân hàng và lấy thông tin giao dịch"
              />
      
      </div>
      
    <div className="bounder">

      <div >

      {
        isAlertVisible && disableClick===false?     <Alert className="alertsuccess" variant="filled" color="success" severity="success">Liên kết với tài khoản ngân hàng thành công  </Alert> : 
        // <Alert className="alertsuccess" variant="filled" color="error" severity="error">Có lỗi xảy ra, hãy liên kết tài khoản ngân hàng  </Alert>
        ""
}
      
      { 
        isAlertVisible2 && transaction===true?     <Alert className="alertsuccess" variant="filled" color="success" severity="success">Lấy thông tin giao dịch thành công  </Alert> : 
        // <Alert className="alertsuccess" variant="filled" color="error" severity="error">Có lỗi xảy ra, hãy liên kết tài khoản ngân hàng  </Alert>
      ""
      } 

    
{/* 
      { 
        // isAlertVisible3 &&
         disableClick===true  && transaction===true?      : 
        ""
      } */}

      {shown ? 
      <IframeModal src = {srcurl}/> : null}
      </div>
      <div className= "step1bounder" >
      <label className= "step1"> Bước 1: Trước khi thực hiện lấy thông tin giao dịch, quý khách vui lòng liên kết tài khoản ngân hàng </label>
      </div>
      <div>


      <button 
      // onClick={() => setShown(!shown)}
      id="button1"
      className = "buttonlink"
      onClick={handleClick}

      
      >

      Liên kết ngân hàng

      </button>

      </div>

        {/* <div>
      {disableClick===true?
            <b>
            <button 
            // onClick={() => setShown(!shown)}
            id="button2"
         
            className = "buttonlinktransaction"
            onClick={handleClickTransaction}
            >
      
            Lấy giao dịch
            </button>
            </b>
      
      :
            <a>
            <button 
            // onClick={() => setShown(!shown)}
            id="button2"
        
            className = "buttonlinktransaction"
            onClick={handleClickTransaction}
            >

            Lấy thông tin giao dịch
            </button>
            </a>
      }
      </div> */}

<div className= "step2bounder" >
      <label className= "step1"> Bước 2: Sau khi đã liên kết tài khoản thành công, quý khách có thể lấy thông tin giao dịch </label>
      </div>
      <div>


      <button 
      // onClick={() => setShown(!shown)}
      id="button2"
   
      className = "buttonlinktransaction"
      onClick={handleClickTransaction}
      >

      {disableClick === true? 
                      
            <b>
            Lấy giao dịch
            </b>


      :

            <a>
            Lấy thông tin giao dịch
            </a>
        


      }

      </button>

      </div>

      <div>
      {transaction===true?

      <div className="transactions">
      {/* <h1>accountName: {dataArray.length} {DataTaskView.length} </h1> */}
      <div  className="header-transaction"  >
      <Header title="THÔNG TIN GIAO DỊCH"/>
      </div>
      <Box
        m="20px 0 20px 0"
        // height="75vh"
        display="flex"
        className="glassmorphism"
        gap="20px"
        gridColumn="span 4"
        gridRow="span 2"
        flexDirection="column"
        p="15px"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            
            backgroundColor: colors.blueAccent[700],
            // backgroundColor: "#3e4396",
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiCheckbox-root": {
            color: `${colors.greenAccent[200]} !important`,
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`,
          },
        }}
      >
        {/* <DataGrid
          rows={mockDataTasks}
          columns={columns}
          components={{ Toolbar: GridToolbar }}
        /> */}

        <TableContainer sx={{ maxHeight: "70vh" }} >
          <Table aria-label="simple table" stickyHeader >
            <TableHead >
              <TableRow
              className="TableRowCSS"
                // sx={{
                //  backgroundColor: colors.blueAccent[800],
                //   mx: "30px",
                // }}
              >
                <TableCell>
                  <Typography  className="TableRowCSS">Tên tài khoản</Typography>
                </TableCell>
                <TableCell>
                  <Typography className="TableRowCSS">Số tài khoản</Typography>
                </TableCell>
                <TableCell align="left">
                  <Typography className="TableRowCSS">Thời gian giao dịch</Typography>
                </TableCell>
                <TableCell align="left">
                  <Typography className="TableRowCSS">Biến động số dư</Typography>
                </TableCell>
                <TableCell align="left">
                  <Typography className="TableRowCSS">Mô tả giao dịch</Typography>
                </TableCell>
                <TableCell align="left">
                  <Typography className="TableRowCSS">Số dư hiện tại</Typography>
                </TableCell>
                {/* <TableCell>
                  <Typography variant="h6">Phương Tiện</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">Giờ Bắt Đầu</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">Ngày</Typography>
                </TableCell> */}
                {/* <TableCell>
                  <Typography variant="h6">Ghi Chú</Typography>
                </TableCell> */}
              </TableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                ? dataArray.slice(
                    page * rowsPerPage,
                    page * rowsPerPage + rowsPerPage
                  )
                : dataArray
              ).map((row) => (
                <TableRow key={row.id}>
                  {/* <TableCell className={classes.tableCell}>{row.locate}</TableCell> */}
                  <TableCell className={classes.tableCell}>
                    {accountName}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {row.accountNumber}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {row.transactionDateTime}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {row.amount}
                  </TableCell>
                  <TableCell
                    className={classes.tableCell}
                    // style={
                    //   row.status === "Đang thực hiện"
                    //     ? { color: colors.greenAccent[400] }
                    //     : row.status === "Chưa thực hiện"
                    //     ? { color: colors.redAccent[400] }
                    //     : { color: colors.grey[300] }
                    // }
                  >
                    {row.description}

                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {row.runningBalance}
                  </TableCell>
                  {/* <TableCell className={classes.tableCell}>
                    {row.startTime}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {row.Date}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {row.note}
                  </TableCell> */}
                </TableRow>
              ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter
              sx={{
                position: "sticky",
                bottom: 0,
                zIndex: 1,
              }}
            >
              {/* <TableRow align="left"> */}

              {/* </TableRow> */}
            </TableFooter>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 7, 10, { label: "Tất cả", value: -1 }]}
          colSpan={12}
          count={dataArray.length}
          rowsPerPage={rowsPerPage}
          page={page}
          SelectProps={{
            inputProps: {
              "aria-label": "Rows per page:",
            },
            native: true,
          }}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          ActionsComponent={TablePaginationActions}
        />
      </Box>
    

 
      </div>
      // <Box m ="20px">


        // </Box>

      : 
      null}
        </div>

        
    </div>


      
      


    </Box>

  );
}

export default App;
