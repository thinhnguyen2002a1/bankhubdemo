const express = require("express");
const app = express();
const MongoClient = require("mongodb").MongoClient;
const cors = require("cors");
const bodyParser = require("body-parser");
// const ObjectId = require('mongodb').ObjectId;
//mongodb+srv://admin:7h0qSgOaUSUPciJK@cluster0.ah9khll.mongodb.net/
//mongodb+srv://admin:7h0qSgOaUSUPciJK@cluster0.ah9khll.mongodb.net/bankhub?retryWrites=true&w=majority

app.use(cors());
app.use(bodyParser.json());
app.use(express.json());

let xclientid = "adcf7089-0a81-11ee-b8b7-42010a40001b";
let xsecretkey = "adcf70a5-0a81-11ee-b8b7-42010a40001b";
let url = "http://localhost:3000";
var grantToken;
//
const axios = require("axios");

// var dbo = db.db("bankhub");

// dbo.createCollection("person", (err,res) =>{
//   if(err) throw err;

//   console.log("Create collection success");
// });

app.get("/bankhub/token", async (req, res) => {
  //
  let data = JSON.stringify({
    scopes: "transaction",
    redirectUri: "http://localhost:3000",
    language: "vi",
  });

  let config = {
    method: "post",
    maxBodyLength: Infinity,
    url: "https://sandbox.bankhub.dev/grant/token",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      "x-client-id": xclientid,
      "x-secret-key": xsecretkey,
    },
    data: data,
  };

  axios(config)
    .then((response) => {
      console.log(JSON.stringify(response.data));
      grantToken = response.data.grantToken;
      let iframeurl =
        "https://dev.link.bankhub.dev?grantToken=" +
        grantToken +
        "&redirectUri=" +
        url +
        "&iframe=true";
      return res.json({ iframeurl });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).send("Something went wrong.");
    });
});

app.post("/bankhub/exchangeToken", async (req,res) =>
  {
    const publicToken = req.query.publicToken;
    // console.log(publicToken);
    const axios = require('axios');

let data = JSON.stringify({
  "publicToken": publicToken
});

let config = {
  method: 'post',
maxBodyLength: Infinity,
  url: 'https://sandbox.bankhub.dev/grant/exchange',
  headers: { 
    'Content-Type': 'application/json', 
    'Accept': 'application/json', 
    'x-client-id': xclientid, 
    'x-secret-key': xsecretkey
  },
  data : data
};

axios(config)
.then((response) => {
  console.log(JSON.stringify(response.data));
  console.log(JSON.stringify(response.data.accessToken));
  let accessToken = response.data.accessToken;
  return res.json(accessToken);
});
  });


app.post("/bankhub/transaction", async (req, res) => {
  //
  const accessToken = req.query.accessToken;
  const axios = require('axios');

  let config = {
    method: 'get',
  maxBodyLength: Infinity,
    url: 'https://sandbox.bankhub.dev/transactions',
    headers: { 
      'Accept': 'application/json', 
      'x-client-id': xclientid, 
      'x-secret-key': xsecretkey, 
      'Authorization': accessToken,
    }
  };
  
  axios(config)
  .then((response) => {
    console.log(JSON.stringify(response.data));
    let dataTransaction = response.data;
    return res.json(dataTransaction);
  })
  .catch((error) => {
    console.log(error);
  });
});




const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
