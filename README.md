# BankhubDemo


Version 1.0: 
- Send and request GrantToken
- Used GrantToken return iframeurl
- Post PublicToken from UI to Server

Version 1.1:
- Exchange public token and access token
- Get AccessToken from bankhub to server

Version 1.2:
- Save AccessToken from server to LocalStorage
- Get data transaction from bankhub

Version 1.3:
- Complete get data and display on table 
- Complete UI